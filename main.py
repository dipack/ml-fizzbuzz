#!/usr/bin/env python3
import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping, TensorBoard
from keras.utils import np_utils

def fizz_buzz(n):
    # Logic Explanation
    if n % 3 == 0 and n % 5 == 0:
        return 'fizzbuzz'
    elif n % 3 == 0:
        return 'fizz'
    elif n % 5 == 0:
        return 'buzz'
    else:
        return 'other'

def create_input_csv(start,end,filename):
    inputData   = []
    outputData  = []
    # Why do we need training Data?
    # We need training data to actually train the model
    for i in range(start,end):
        inputData.append(i)
        outputData.append(fizz_buzz(i))
    # Why Dataframe?
    # A dataframe is used because it can be used like a JSON object in JS, to store objects of different types
    # with string/number keys
    dataset = {}
    dataset["input"]  = inputData
    dataset["label"] = outputData
    # Writing to csv
    pd.DataFrame(dataset).to_csv(filename)
    print(filename, "Created!")
    return

def process_dataset(dataset):
    # Why do we have to process?
    # We need to process the data to convert them into their binary representations
    # We need to process the labels to convert the text labels
    # into single integer arrays, either [0], [1], [2], or [3]
    data   = dataset['input'].values
    labels = dataset['label'].values
    processedData  = encode_data(data)
    processedLabel = encode_label(labels)
    return processedData, processedLabel

def encode_data(data):
    processedData = []
    for dataInstance in data:
        # Why do we have number 10?
        # We have the number 10, as we have a dataset of integers upto 1000,
        # which means we will have at most 10 bits, as 2 ** 10 = 1024
        # We are first right shifting the integer by 0..9 bits, and then 'AND'ing it with 1,
        # to get the reversed bit sequence of the binary rep. of the integer represented by `dataInstance`O
        processedData.append([dataInstance >> d & 1 for d in range(10)])
    return np.array(processedData)

def encode_label(labels):
    processedLabel = []
    for labelInstance in labels:
        if(labelInstance == "fizzbuzz"):
            # Fizzbuzz
            processedLabel.append([3])
        elif(labelInstance == "fizz"):
            # Fizz
            processedLabel.append([1])
        elif(labelInstance == "buzz"):
            # Buzz
            processedLabel.append([2])
        else:
            # Other
            processedLabel.append([0])
    # This takes the vector of labels for the data, and converts to a binary class matrix
    # Where each column represents a label
    return np_utils.to_categorical(np.array(processedLabel),4)

def decode_label(encodedLabel):
    if encodedLabel == 0:
        return "other"
    elif encodedLabel == 1:
        return "fizz"
    elif encodedLabel == 2:
        return "buzz"
    elif encodedLabel == 3:
        return "fizzbuzz"

def get_model(first_dense_layer_nodes, second_dense_layer_nodes, input_size,  drop_out):
    # Why do we need a model?
    # We need a model to actually perform our classification for us, on some input data

    # Why use sequential model with layers?
    # We use a sequential model with different layers, as the layers are not used more than once
    model = Sequential()

    # Why use Dense layer and then activation?
    # We use a Dense layer as it linearly connects the input to the output,
    # by way of the activation function supplied in the next line
    model.add(Dense(first_dense_layer_nodes, input_dim=input_size))
    # Activation relu - rectified linear output is used to bring the inputs within a certain range
    # In this case, we simply remove all negative values
    model.add(Activation('relu'))

    # Why dropout?
    # We add a dropout layer to solve the problem of overfitting the input data
    # to the model; it is the probability that an input will be unused, i.e., 'dropped'
    model.add(Dropout(drop_out))

    model.add(Dense(second_dense_layer_nodes))
    # Why Softmax?
    # We use a softmax activation function to bring the values to between 0 and 1, with the sum
    # of the resulting array being 1
    # This is done to prevent extreme values from skewing the operations
    # In this case, the array represents the probability that an input belongs to certain class
    model.add(Activation('softmax'))

    model.summary()

    # Why use categorical_crossentropy?
    # We use categorical_crossentropy as 'loss' function, to calculate the difference between
    # what the model computes, and what our testing data shows
    # A 'loss' function is used to correct the model by increasing or decreasing the learning rate
    # A cross entropy loss function is used in cases when the output is a prob. distribution
    # b/w 0 to 1, as in this case is generated from the 'softmax' layer
    from keras import losses
    model.compile(optimizer='adadelta',
            loss=losses.categorical_crossentropy,
            metrics=['accuracy'])

    return model

def build_model():
    input_size = 10
    drop_out = 0.1
    first_dense_layer_nodes  = 512
    second_dense_layer_nodes = 4

    # Create datafiles
    create_input_csv(101,1001,'training.csv')
    create_input_csv(1,101,'testing.csv')

    model = get_model(first_dense_layer_nodes, second_dense_layer_nodes, input_size, drop_out)

    validation_data_split = 0.2
    num_epochs = 10000
    model_batch_size = 128
    tb_batch_size = 32
    early_patience = 100

    # Keras Tensorboard used to visualise of training and test metrics
    tensorboard_cb   = TensorBoard(log_dir='logs', batch_size= tb_batch_size, write_graph=True)
    # Early stopping is a technique that provides guidance on avoiding overfitting;
    # it basically stops training based on a value that it monitors, in this case, it monitors the loss
    # patience is the number of epochs with NO improvement, after which training will stop
    earlystopping_cb = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=early_patience)

    # Read Dataset
    dataset = pd.read_csv('training.csv')

    # Process Dataset
    processedData, processedLabel = process_dataset(dataset)
    history = model.fit(processedData
            , processedLabel
            , validation_split=validation_data_split
            , epochs=num_epochs
            , batch_size=model_batch_size
            , callbacks = [earlystopping_cb, tensorboard_cb]
            )

    import matplotlib.pyplot as plt
    df = pd.DataFrame(history.history)
    # Plot all values, val_loss, acc, loss, etc.
    # df.plot(subplots=True, grid=True, figsize=(9,15))
    df['val_loss'].plot(x='Epoch', y='Loss', subplots=True, grid=True, figsize=(9,15))
    plt.show()

    return model

def test_model(model):
    wrong   = 0
    right   = 0

    testData = pd.read_csv('testing.csv')

    processedTestData  = encode_data(testData['input'].values)
    processedTestLabel = encode_label(testData['label'].values)
    predictedTestLabel = []

    for i,j in zip(processedTestData,processedTestLabel):
        reshaped_i = np.array(i).reshape(-1,10)
        y = model.predict(reshaped_i)
        # print("i: {0}, reshaped: {1}, y: {2}".format(i, reshaped_i, y))
        predictedTestLabel.append(decode_label(y.argmax()))

        if j.argmax() == y.argmax():
            right = right + 1
        else:
            wrong = wrong + 1

    print("Errors: " + str(wrong), " Correct :" + str(right))

    print("Testing Accuracy: " + str(right/(right+wrong)*100))

    output = {}
    output["input"] = testData['input']
    output["label"] = testData['label']
    output["predicted_label"] = predictedTestLabel

    opdf = pd.DataFrame(output)
    opdf.to_csv('output.csv')
    return

def print_submitter_info():
    print("=================================")
    print("UBITName      : dipackpr")
    print("Person number : 50291077")
    print("=================================")
    return

if __name__=='__main__':
    print_submitter_info()
    model = build_model()
    test_model(model)
